# spring-boot-starter-kafka

## 发送消息
```java
@Autowired
private KafkaTemplate<String,String> kafkaTemplate;

public void sendMsg() {
    kafkaTemplate.send("test", "hello", "world");
}
```



## 消费消息
```java
@Component
public class MyListener {

    @KafkaListener(topics = "test", group = "test", ackMode = AckMode.MANUAL)
    public void testListener(ConsumerRecords<String, String> consumerRecords, MessageAck messageAck) {
        consumerRecords.forEach(consumerRecord -> {
            String key = consumerRecord.key();
            String value = consumerRecord.value();
            System.out.println("key = " + key + "; value = " + value);
        });
    // 手动提交
    messageAck.ack();
}

    @KafkaListener(topics = "test", group = "test2", ackMode = AckMode.AUTOMATIC)
    public void testListener2(ConsumerRecords<String, String> consumerRecords) {
        consumerRecords.forEach(consumerRecord -> {
            String key = consumerRecord.key();
            String value = consumerRecord.value();
            System.out.println("key = " + key + "; value = " + value);
        });
    }
}
```


application.yaml
```yaml
hardyma:
  icu:
    kafka:
      bootstrap-servers: localhost:9092
      producer:
        key-serializer-class: org.apache.kafka.common.serialization.StringSerializer
        value-serializer-class: org.apache.kafka.common.serialization.StringSerializer
      consumer:
        key-deserializer-class: org.apache.kafka.common.serialization.StringDeserializer
        value-deserializer-class: org.apache.kafka.common.serialization.StringDeserializer
```