package start;

import com.bunnymaicai.boot.kafka.autoconfigure.KafkaTemplate;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author youshang
 * @date 2021/02/25 17:19
 **/
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    private String topic = "test";

    @GetMapping("/")
    public void hello(@RequestParam(required = false) String key, @RequestParam String value){
        kafkaTemplate.send(topic, key, value, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                System.out.println(metadata);
            }
        });
        System.out.println(" Hello World !!!");
    }
}
