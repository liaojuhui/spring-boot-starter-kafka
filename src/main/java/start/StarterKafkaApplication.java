package start;

import com.bunnymaicai.boot.kafka.autoconfigure.KafkaConfigurationPostProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 *
 * @author youshang
 * @date 2021/02/25 16:57
 **/
@ComponentScan(value = {"com.bunnymaicai.boot.kafka.autoconfigure","start"})
@SpringBootApplication
public class StarterKafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(StarterKafkaApplication.class,args);
    }
    @Bean
    public KafkaConfigurationPostProcessor initKafkaConfigurationPostProcessor(){
        return new KafkaConfigurationPostProcessor();
    }
}
