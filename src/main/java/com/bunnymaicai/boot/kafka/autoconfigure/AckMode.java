package com.bunnymaicai.boot.kafka.autoconfigure;

import lombok.Data;

public enum AckMode {
    /**
     * 自动提交offset
     */
    AUTOMATIC,
    /**
     * 手动提交offset
     */
    MANUAL;

}
