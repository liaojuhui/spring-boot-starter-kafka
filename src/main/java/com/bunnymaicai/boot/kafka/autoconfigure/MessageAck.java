package com.bunnymaicai.boot.kafka.autoconfigure;

/**
 * @author youshang
 */
public interface MessageAck {
    void ack(long currentOffset,int partition);
}
