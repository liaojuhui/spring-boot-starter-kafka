package com.bunnymaicai.boot.kafka.autoconfigure.model;

/**
 *
 * @author youshang
 * @date 2021/08/02 22:57
 **/
public class KafkaConfiguration {
    private String bootstrapServers;
    private KafkaConsumer consumer = new KafkaConsumer();
    private KafkaProducer producer = new KafkaProducer();

    public KafkaConsumer getConsumer() {
        return consumer;
    }

    public KafkaProducer getProducer() {
        return producer;
    }

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public void setConsumer(KafkaConsumer consumer) {
        this.consumer = consumer;
    }

    public void setProducer(KafkaProducer producer) {
        this.producer = producer;
    }
}
