# spring-boot-starter-kafka

#### 介绍
自定义kafka收发消息

#### 软件架构
软件架构说明


#### 安装教程

1.  启动kafka，kafka怎么搭我有另外一个项目中有提到
https://gitee.com/liaojuhui/kafka-learning

#### 使用说明

1.  在项目中需要接受消息的地方添加注解@KafkaListener注解
```java
           @KafkaListener(value = "test",group = "test-group1",ackMode = AckMode.AUTOMATIC)
           public void hello(ConsumerRecords<Object, Object> consumerRecords, MessageAck messageAck){
               for (ConsumerRecord<Object, Object> consumerRecord : consumerRecords) {
           
                    //只有当 AckMode.MANUAL 的时候手动提交才会生效
        //           messageAck.ack(consumerRecord.offset(),consumerRecord.partition());
                   log.info(String.format("消费消息成功 : offset :  %s , partition : %s ",consumerRecord.offset(),consumerRecord.partition()));
               }
           }
```
2.  发送消息使用
```java
        //按照自己的特定需求使用特定的发送方式
        kafkaTemplate.send(topic, key, value, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                System.out.println(metadata);
            }
        });
```
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
